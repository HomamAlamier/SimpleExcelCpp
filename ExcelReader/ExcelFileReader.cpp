#include "ExcelFileReader.h"
#include "ExcelFileTemplate.h"
#define NOT_NULL != nullptr

const std::string fileSheet1 = "temp\\xl\\worksheets\\sheet1.xml";
const std::string fileSharedStrings = "temp\\xl\\sharedStrings.xml";
const std::string charsAllowedInNumber = "1234567890-+";

bool extractFiles(std::string filename)
{
    std::string cmd = "minizip.exe -d temp -o -x " + filename;
    return std::system(cmd.c_str()) == 0;
}



bool isNumber(std::string s)
{
    for (char c : s)
    {
        bool allowed = false;
        for (char c2 : charsAllowedInNumber)
        {
            if (c2 == c)
            {
                allowed = true;
                break;
            }
        }
        if (!allowed)
            return false;
    }
    return true;
}

std::vector<std::string> split(std::string& x, char y)
{
    std::string s;
    std::vector<std::string> sl;
    for (char c : x)
    {
        if (c == y)
        {
            sl.push_back(s);
            s = "";
        }
        else
            s += c;
    }
    sl.push_back(s);
    return sl;
}

ExcelFileReader::ExcelFileReader(std::string filename)
{
    extractFiles(filename);
    readSharedStrings();
    readSheet1();
}

ExcelFileReader::ExcelFileReader() :
    _rowCount(0)
{

}

void ExcelFileReader::addData(unsigned row, unsigned col, std::string data)
{
    DataItem item;
    if (row > _rowCount)
        _rowCount++;
    item.row = row;
    item.column = col;
    if (isNumber(data))
    {
        item.value = data;
    }
    else
    {
        _sharedStrings.push_back(data);
        item.value = std::to_string(_sharedStrings.size() - 1);
        item.type = SharedString;
    }
    _items.push_back(item);
}

void ExcelFileReader::readSheet1()
{
    std::fstream s(fileSheet1, std::fstream::in);
    char c;
    size_t currentRow = 0;
    DataItem* lastitem = nullptr;

    while (s.read(&c, 1).gcount() > 0)
    {
        if (c == '<')
        {
            std::string tag;
            while (s.read(&c, 1).gcount() > 0 && c != '>')
            {
                tag += c;
            }

            std::vector<std::string> list = split(tag, ' ');
            if (list.size() > 0)
            {
                if (list[0] == "row")
                {
                    _rowCount++;
                    for (std::string x : list)
                    {
                        if (x.find("r=") < x.size())
                        {
                            size_t s = x.find('\"') + 1;
                            size_t e = x.find('\"', s);
                            std::string r = x.substr(s, e - s);
                            currentRow = std::atoi(r.c_str());
                        }
                    }
                }
                else if (list[0] == "c")
                {
                    DataItem item;
                    item.row = currentRow;
                    for (std::string x : list)
                    {
                        if (x.find("s=") < x.size())
                        {
                            size_t s = x.find('\"') + 1;
                            size_t e = x.find('\"', s);
                            std::string r = x.substr(s, e - s);
                            item.column = std::atoi(r.c_str());
                        }
                        if (x.find("t=") < x.size())
                        {
                            size_t s = x.find('\"') + 1;
                            size_t e = x.find('\"', s);
                            std::string t = x.substr(s, e - s);
                            if (t == "s")
                                item.type = SharedString;
                        }
                    }

                    //bool isValid = false;
                    //DataItem* validItem = nullptr;
                    //for (size_t i = 0; i < _items.size(); i++)
                    //{
                    //    DataItem* it = &_items[i];
                    //    if (it->row == currentRow && it->column == item.column)
                    //    {
                    //        isValid = true;
                    //        validItem = it;
                    //        break;
                    //    }
                    //}

                    //if (!isValid) 
                        _items.push_back(item);
                    //else if (validItem NOT_NULL)
                    //    validItem->value += " " + item.value;

                    lastitem = &_items[_items.size() - 1];
                }
                else if (list[0] == "v")
                {
                    std::string va;
                    while (s.read(&c, 1).gcount() > 0 && c != '<')
                    {
                        va += c;
                    }
                    if (lastitem NOT_NULL) 
                    {
                        if (lastitem->type == SharedString)
                        {
                            int index = std::atoi(va.c_str());
                            if (index < _sharedStrings.size() && index > 0)
                                va = _sharedStrings[index];
                        }
                            
                        lastitem->value = va;
                    }
                }
            }
        }
    }
    s.close();
}

void ExcelFileReader::readSharedStrings()
{
    std::fstream s(fileSharedStrings, std::fstream::in);
    char c;
    size_t currentRow = 0;
    DataItem* lastitem = nullptr;

    while (s.read(&c, 1).gcount() > 0)
    {
        if (c == '<')
        {
            std::string tag;
            while (s.read(&c, 1).gcount() > 0 && c != '>')
            {
                tag += c;
            }

            std::vector<std::string> list = split(tag, ' ');
            if (list.size() > 0)
            {
                if (list[0] == "t")
                {
                    std::string va;
                    while (s.read(&c, 1).gcount() > 0 && c != '<')
                    {
                        va += c;
                    }
                    _sharedStrings.push_back(va);
                }
            }
        }
    }
    s.close();
}

std::string ExcelFileReader::getValueAt(int r, int c)
{
    DataItem item;
    for (auto i : _items)
    {
        if (i.row == r && i.column == c)
        {
            return i.value;
        }
    }
}

std::string ExcelFileReader::toString()
{
    std::string tmp;
    int lr = _items[0].row;
    for (auto i : _items)
    {
        if (lr != i.row)
        {
            lr = i.row;
            tmp += "\r\n";
        }

        tmp += i.value + "\t";
    }
    return tmp;
}

void ExcelFileReader::CreateBlankExcelFile(std::string filename)
{
    std::string cmd = "copy ";
    cmd += EXCEL_TEMPLATE_BLANK;
    cmd += filename;
    std::system(cmd.c_str());
}