#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>



class ExcelFileReader
{
public:
    ExcelFileReader(std::string filename);
    ExcelFileReader();
    std::string getValueAt(int row, int column);
    std::string filename() { return _filename;  }
    std::string toString();

    unsigned rowCount() { return _rowCount; }
    void addData(unsigned row, unsigned col, std::string data);

    static void CreateBlankExcelFile(std::string filename);

private:

    void readSheet1();
    void readSharedStrings();
    enum DataItemType
    {
        Number = 0,
        SharedString = 1
    };
    struct DataItem
    {
        int row = 0;
        int column = 0;
        std::string value;
        DataItemType type = Number;
    };

    unsigned _rowCount;

    std::vector<DataItem> _items;
    std::vector<std::string> _sharedStrings;
    std::string _filename;
};

